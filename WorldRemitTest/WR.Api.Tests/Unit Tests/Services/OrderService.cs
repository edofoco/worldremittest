﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using WR.Api.Exceptions;
using WR.eCommerceService.lib;
using WR.eCommerceService.lib.Models;
using WR.OrderProcessor.lib;

namespace WR.Api.Tests.Unit_Tests.Services
{
    [TestClass]
    public class OrderService
    {
        private static Mock<IECommerceService> _mockECommerceService;
        private static Mock<IOrderProcessor> _mockOrderProcessor;

        private static Api.Services.OrderService SUT;

        private User CreateDummyUser()
        {
            return new User
            {
                UserId = 1,
                Name = "John",
                Address_1 = "Oxford St",
                Postcode = "W1",
                City = "City",
                Country = "UK"
            };

        }

        private List<Product> CreateDummyProducts()
        {
            return new List<Product> {
                    new Product {ProductId = 1, Name = "Book1", Cost = 10.5m, ProductType = new ProductType { Type = "Book", IsPhysical = true} },
                    new Product {ProductId = 2, Name = "Book2", Cost = 15.5m, ProductType = new ProductType { Type = "Book", IsPhysical = true} }
                };
        }

        private Membership CreateDummyMembership()
        {
            return new Membership
            {
                MembershipId = 1,
                Name = "Premium",
                Cost = 30m
            };
        }

        [TestMethod]
        public void OrderService_CreateOrderWithOnlyProducts_VerifyProcessOrderCalledWithCorrectProducts() {

            _mockECommerceService = new Mock<IECommerceService>();
            _mockOrderProcessor = new Mock<IOrderProcessor>();

            var user = CreateDummyUser();
            var products = CreateDummyProducts();

            _mockECommerceService.Setup(m => m.GetUser(It.IsAny<int>())).Returns(user);
            _mockECommerceService.Setup(m => m.GetProduct(1)).Returns(products[0]);
            _mockECommerceService.Setup(m => m.GetProduct(2)).Returns(products[1]);

            SUT = new Api.Services.OrderService(_mockECommerceService.Object, _mockOrderProcessor.Object);
            SUT.CreateOrder(1, new List<int> { 1, 2 }, null);


            _mockOrderProcessor.Verify(m => m.ProcessOrder(user, products, null), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(BadRequestException))]
        public void OrderService_CreateOrderInvalidUser_AssertBadRequestExceptionThrown()
        {

            _mockECommerceService = new Mock<IECommerceService>();
            _mockOrderProcessor = new Mock<IOrderProcessor>();

            _mockECommerceService.Setup(m => m.GetUser(It.IsAny<int>())).Returns((User) null);

            SUT = new Api.Services.OrderService(_mockECommerceService.Object, _mockOrderProcessor.Object);
            SUT.CreateOrder(1, new List<int> { 1, 2 }, null);

        }

        [TestMethod]
        [ExpectedException(typeof(BadRequestException))]
        public void OrderService_CreateOrderInvalidProduct_AssertBadRequestExceptionThrown()
        {

            _mockECommerceService = new Mock<IECommerceService>();
            _mockOrderProcessor = new Mock<IOrderProcessor>();

            var user = CreateDummyUser();
            var products = CreateDummyProducts();

            _mockECommerceService.Setup(m => m.GetUser(It.IsAny<int>())).Returns(user);
            _mockECommerceService.Setup(m => m.GetProduct(It.IsAny<int>())).Returns((Product)null);

            SUT = new Api.Services.OrderService(_mockECommerceService.Object, _mockOrderProcessor.Object);
            SUT.CreateOrder(1, new List<int> { 1, 2 }, null);

        }

        [TestMethod]
        [ExpectedException(typeof(BadRequestException))]
        public void OrderService_CreateOrderInvalidMembership_AssertBadRequestExceptionThrown()
        {

            _mockECommerceService = new Mock<IECommerceService>();
            _mockOrderProcessor = new Mock<IOrderProcessor>();

            _mockECommerceService.Setup(m => m.GetMembership(It.IsAny<int>())).Returns((Membership)null);

            var user = CreateDummyUser();
            var products = CreateDummyProducts();

            _mockECommerceService.Setup(m => m.GetUser(It.IsAny<int>())).Returns(user);
            _mockECommerceService.Setup(m => m.GetProduct(It.IsAny<int>())).Returns((Product)null);

            SUT = new Api.Services.OrderService(_mockECommerceService.Object, _mockOrderProcessor.Object);
            SUT.CreateOrder(1, new List<int> { 1, 2 }, 1);


        }

    }
}
