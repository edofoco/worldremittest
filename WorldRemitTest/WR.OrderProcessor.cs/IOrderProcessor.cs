﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WR.eCommerceService.lib.Models;

namespace WR.OrderProcessor.lib
{
    public interface IOrderProcessor
    {
        Order ProcessOrder(User user, List<Product> Products = null, Membership membership = null);
    }
}
