﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WR.eCommerceService.lib.Models;

namespace WR.OrderProcessor.lib.Services
{
    public interface IShippingSlipGenerator
    {
       Task GenerateShippingSlip(Order order);
    }
}
