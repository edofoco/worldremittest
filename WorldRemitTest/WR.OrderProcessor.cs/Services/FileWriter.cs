﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WR.OrderProcessor.lib.Services
{
    public class FileWriter: IShippingSlipWriter
    {
        private readonly string _basePath;

        public FileWriter(string basePath)
        {
            _basePath = basePath;
        }

        public async Task Write(string text, string filename)
        {
             System.IO.File.WriteAllText($@"{_basePath}/{filename}.txt", text);
        }
    }
}
