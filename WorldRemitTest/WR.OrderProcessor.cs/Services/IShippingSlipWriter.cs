﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WR.OrderProcessor.lib.Services
{
    public interface IShippingSlipWriter
    {
        Task Write(string text, string filename);
    }
}
