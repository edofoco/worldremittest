﻿using System;
using System.Threading.Tasks;
using WR.eCommerceService.lib.Models;

namespace WR.OrderProcessor.lib.Services
{
    public class SimpleTextShippingSlipGenerator : IShippingSlipGenerator
    {
        private IShippingSlipWriter _writer;

        public SimpleTextShippingSlipGenerator(IShippingSlipWriter writer)
        {
            _writer = writer;
        }

        private void SuccessAction() {
            //Do Something - Maybe send an alert?
        }

        private async Task ErrorAction(string text, string filename)
        {
            var success = false;
            var tentative = 0;

            while (!success && tentative < 3) {
                try
                {
                    await _writer.Write(text, filename).ConfigureAwait(false);
                    success = true;
                    SuccessAction();
                }
                catch (Exception e) {
                    //Log Exception
                    tentative++;
                }
            }
        }

        public async Task GenerateShippingSlip(Order order)
        {
            var text = $"World Remit - Shipping Slip\r\n\r\n" +
                $"Order Number: {order.OrderId}" +
                $"\r\nCustomerId: {order.User.UserId}," +
                $"\r\nName: {order.User.Name}" +
                $"\r\nDelivery Address: {order.User.Address_1}, {order.User.Postcode}, {order.User.City}, {order.User.Country}" +
                $"\r\n\r\nItems To Be Delivered:\r\n";


            foreach (var product in order.Products)
            {
                if (product.ProductType.IsPhysical)
                {
                    text += $"{product.Name} | {product.ProductType.Type} | {product.Cost}£\r\n";
                }
            }

            var filename = $"order_{order.OrderId}_{Guid.NewGuid().ToString()}_shippingslip.txt";

            try
            {
                await _writer.Write(text, filename).ConfigureAwait(false);
                SuccessAction();
            }
            catch (Exception e) {
                await ErrorAction(text, filename).ConfigureAwait(false);
            }

        }
    }
}
