﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WR.eCommerceService.lib;
using WR.eCommerceService.lib.Models;
using WR.OrderProcessor.lib.Services;

namespace WR.OrderProcessor.lib
{
    public class OrderProcessor: IOrderProcessor
    {
        IECommerceService _eCommerceService;
        IShippingSlipGenerator _shippingListGenerator;

        public OrderProcessor(IECommerceService eCommerceService, IShippingSlipGenerator shippingListGenerator)
        {
            _eCommerceService = eCommerceService;
            _shippingListGenerator = shippingListGenerator;
        }

        private Order AddProducts(Order order, List<Product> products)
        {
            if (products != null)
            {
                foreach (var product in products)
                {
                    order.Products.Add(product);
                    order.Total += product.Cost;
                }
            }
            return order;
        }

        private Order AddMembership(Order order, Membership membership)
        {
            order.MembershipId = membership.MembershipId;
            order.Total += membership.Cost;
            return order;
        }

        private Order CreateOrder(User user, List<Product> products = null, Membership membership = null) {
            var order = new Order
            {
                UserId = user.UserId,
                Products = new List<Product>()
            };

            order = AddProducts(order, products);

            if (membership != null)
            {
                order = AddMembership(order, membership);
                user.MembershipId = membership.MembershipId;
            }

            _eCommerceService.CreateOrder(order);

            return order;
        }

        private void GenerateShippingList(Order order) {
            var physicalProduct = order.Products.FirstOrDefault(p => p.ProductType.IsPhysical);
            if (physicalProduct != null)
            {
                Task.Run(() => _shippingListGenerator.GenerateShippingSlip(order));
            }
        }

        public Order ProcessOrder(User user, List<Product> products = null, Membership membership = null)
        {
            var order = CreateOrder(user, products, membership);

            if (user.Orders == null)
            {
                user.Orders = new List<Order>();
            }

            user.Orders.Add(order);
            _eCommerceService.UpdateUser(user);

            GenerateShippingList(order);

            return order;
        }
    }
}
