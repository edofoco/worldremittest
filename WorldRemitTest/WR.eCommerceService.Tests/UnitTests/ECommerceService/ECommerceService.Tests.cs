﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using WR.eCommerceService.lib.DAL;
using WR.eCommerceService.lib.Models;

namespace WR.eCommerceService.Tests.UnitTests.ECommerceService
{
    [TestClass]
    public class GenericContext
    {

        private static Mock<IUnitOfWork> _mockUow;

        private static lib.ECommerceService SUT;
        private static Mock<IGenericRepository<Product>> _mockProductRepository;
        private static Mock<IGenericRepository<Order>> _mockOrderRepository;
        private static Mock<IGenericRepository<Membership>> _mockMembershipRepository;
        private static Mock<IGenericRepository<User>> _mockUserRepository;
        private static Mock<IGenericRepository<ProductType>> _mockProductTypeRepository;

        private void Setup()
        {
            _mockProductRepository = new Mock<IGenericRepository<Product>>();
            _mockOrderRepository = new Mock<IGenericRepository<Order>>();
            _mockMembershipRepository = new Mock<IGenericRepository<Membership>>();
            _mockUserRepository = new Mock<IGenericRepository<User>>();
            _mockProductTypeRepository = new Mock<IGenericRepository<ProductType>>();

            _mockUow = new Mock<IUnitOfWork>();
        }

        [TestMethod]
        public void ECommerceService_Products_ReturnsAListOfProducts()
        {
            Setup();

            var products = new List<Product>
            {
                new Product { Name = "Jungle Book", ProductTypeId = 1 },
                new Product { Name = "Jungle Video", ProductTypeId = 2 },
                new Product { Name = "Another Book", ProductTypeId = 1 }
            };
			
            _mockProductRepository.Setup(m => m.Get(It.IsAny<string>()) ).Returns(products.ToList());
            _mockUow.Setup(m => m.ProductRepository).Returns(_mockProductRepository.Object);

            SUT = new lib.ECommerceService(_mockUow.Object);
            var result = SUT.Products;

            CollectionAssert.AreEqual(products, result);
        }

        [TestMethod]
        public void ECommerceService_GetProductById_ReturnsAProductWithProductType()
        {
            Setup();

            var product = new Product { ProductId = 1, Name = "Jungle Book", ProductTypeId = 1 };
            var productType = new ProductType { Type = "Book", ProductTypeId = 1 };
            var expectedResult = product;
            expectedResult.ProductType = productType;

            _mockProductRepository.Setup(m => m.Get(It.IsAny<string>())).Returns(new List<Product> { product });
            
            _mockUow.Setup(m => m.ProductRepository).Returns(_mockProductRepository.Object);
            _mockUow.Setup(m => m.ProductTypeRepository).Returns(_mockProductTypeRepository.Object);

            SUT = new lib.ECommerceService(_mockUow.Object);
			var result = SUT.GetProduct(1);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void ECommerceService_GetUserById_ReturnsAUserWithMembership()
        {
            Setup();

            var user = new User {UserId = 1, Name = "John", MembershipId = 1 };
            var membership = new Membership { Name = "Book", MembershipId = 1 };
            var expectedResult = user;
            expectedResult.Membership = membership;

            _mockUserRepository.Setup(m => m.Get(It.IsAny<string>())).Returns(new List<User> { user });

            _mockUow.Setup(m => m.UserRepository).Returns(_mockUserRepository.Object);
            _mockUow.Setup(m => m.MembershipRepository).Returns(_mockMembershipRepository.Object);

            SUT = new lib.ECommerceService(_mockUow.Object);
            var result = SUT.GetUser(1);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
		public void ECommerceService_CreateOrder_AssertInsertAndSaveAreCalled()
        {
            Setup();

            var order = new Order();

			_mockOrderRepository.Setup(m => m.Insert(It.IsAny<Order>()));
			_mockUow.Setup(m => m.OrderRepository).Returns(_mockOrderRepository.Object);

            SUT = new lib.ECommerceService(_mockUow.Object);
            SUT.CreateOrder(order);

            _mockOrderRepository.Verify(m => m.Insert(It.IsAny<Order>()), Times.Once());
            _mockUow.Verify(m => m.Save(), Times.Once());
        }

        [TestMethod]
        public void ECommerceService_UpdateUser_AssertUpdateAndSaveAreCalled()
        {
            Setup();

            var user = new User();

            _mockUserRepository.Setup(m => m.Insert(It.IsAny<User>()));
            _mockUow.Setup(m => m.UserRepository).Returns(_mockUserRepository.Object);

            SUT = new lib.ECommerceService(_mockUow.Object);
            SUT.UpdateUser(user);

            _mockUserRepository.Verify(m => m.Update(It.IsAny<User>()), Times.Once());
            _mockUow.Verify(m => m.Save(), Times.Once());
        }

    }
}
