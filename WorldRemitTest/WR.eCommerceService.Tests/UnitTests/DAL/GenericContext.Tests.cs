﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using WR.eCommerceService.lib.DAL;
using WR.eCommerceService.lib.Models;

namespace WR.eCommerceService.Tests.UnitTests.DAL
{
    [TestClass]
    public class GenericContext
    {

        private static Mock<DbSet<Product>> _mockDbSet;
        private static Mock<StoreContext> _mockContext;

        private static GenericRepository<Product> SUT;

        [TestMethod]
        public void GenericRepository_GetAllWithProperty_CallsDbSetIncludeWithProperty()
        {

            var products = new List<Product>
            {
                new Product { Name = "Jungle Book", ProductTypeId = 1 },
                new Product { Name = "Jungle Video", ProductTypeId = 2 },
                new Product { Name = "Another Book", ProductTypeId = 1 }
            };

            var data = products.AsQueryable();

            _mockDbSet = new Mock<DbSet<Product>>();
            _mockContext = new Mock<StoreContext>();

            _mockDbSet.As<IQueryable<Product>>().Setup(m => m.Provider).Returns(data.Provider);
            _mockDbSet.As<IQueryable<Product>>().Setup(m => m.Expression).Returns(data.Expression);
            _mockDbSet.As<IQueryable<Product>>().Setup(m => m.ElementType).Returns(data.ElementType);
            _mockDbSet.As<IQueryable<Product>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            _mockDbSet.Setup(m => m.Include("type")).Returns(_mockDbSet.Object);
            _mockDbSet.Setup(m => m.Include("other")).Returns(_mockDbSet.Object);

            _mockContext.Setup(m => m.Set<Product>()).Returns(_mockDbSet.Object);
           
            SUT = new GenericRepository<Product>(_mockContext.Object);
            var result = SUT.Get(includeProperties: "type,other").ToList();

            _mockDbSet.Verify(m => m.Include(It.IsAny<string>()), Times.Exactly(2));

        }

    }
}
