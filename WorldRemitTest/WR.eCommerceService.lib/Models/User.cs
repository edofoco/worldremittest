﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WR.eCommerceService.lib.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Address_1 { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        [ForeignKey("Membership")]
        public int? MembershipId { get; set; }

        public virtual Membership Membership { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
