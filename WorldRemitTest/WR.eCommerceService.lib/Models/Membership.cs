﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WR.eCommerceService.lib.Models
{
  
    public class Membership
    {
        [Key]
        public int MembershipId { get; set; }
        public string Name { get; set; }
        public decimal Cost { get; set; }

        public ICollection<MembershipPermissions> Permissions { get; set; }
    }
}
