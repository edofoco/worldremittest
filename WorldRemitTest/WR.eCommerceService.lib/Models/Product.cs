﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WR.eCommerceService.lib.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        public string Name { get; set; }
        public decimal Cost { get; set; }
        [ForeignKey("ProductType")]
        public int ProductTypeId { get; set; }

        public ProductType ProductType { get; set; }
    }
}
