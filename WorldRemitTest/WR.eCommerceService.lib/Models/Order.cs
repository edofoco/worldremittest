﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WR.eCommerceService.lib.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public decimal Total { get; set; } = 0;
        [ForeignKey("User")]
        public int UserId { get; set; }
        public int? MembershipId { get; set; }

        public User User { get; set; }
        public Membership Membership { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
