﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WR.eCommerceService.lib.Models
{
    public class MembershipPermissions
    {
        [Key]
        public int MembershipPermissionId { get; set; }
        public string Permission { get; set; }

    }
}
