﻿using System.Collections.Generic;
using System.Linq;
using WR.eCommerceService.lib.DAL;
using WR.eCommerceService.lib.Models;

namespace WR.eCommerceService.lib
{
    public class ECommerceService : IECommerceService
    {

        private readonly IUnitOfWork _uow;

        public ECommerceService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public List<Product> Products => _uow.ProductRepository.Get(includeProperties: "ProductType").ToList();
        public List<Membership> Memberships => _uow.MembershipRepository.Get(includeProperties: "permissions").ToList();
        public List<Order> Orders => _uow.OrderRepository.Get(includeProperties: "products.producttype,membership.permissions").ToList();
        public List<User> Users => _uow.UserRepository.Get(includeProperties: "membership.permissions,orders").ToList();
        
        public Product GetProduct(int id) {
           return _uow.ProductRepository.Get("ProductType")
                  .Where(p => p.ProductId == id)
                  .FirstOrDefault();
        }

        public Membership GetMembership(int id){
           return _uow.MembershipRepository.Get("permissions")
                .Where( m => m.MembershipId == id)
                .FirstOrDefault();
        }

        public User GetUser(int id){
            return _uow.UserRepository.Get("membership")
                .Where(u => u.UserId == id)
                .FirstOrDefault();
        }

        public void CreateOrder(Order order) {
            _uow.OrderRepository.Insert(order);
            _uow.Save();
        }

        public void UpdateUser(User user) {
            _uow.UserRepository.Update(user);
            _uow.Save();
        }

    }
}
