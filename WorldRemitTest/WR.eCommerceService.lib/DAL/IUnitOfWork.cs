﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WR.eCommerceService.lib.Models;

namespace WR.eCommerceService.lib.DAL
{
    public interface IUnitOfWork
    {
        IGenericRepository<Product> ProductRepository { get; }
        IGenericRepository<User> UserRepository { get; }
        IGenericRepository<Membership> MembershipRepository { get; }
        IGenericRepository<Order> OrderRepository { get; }
        IGenericRepository<ProductType> ProductTypeRepository { get;  }
        IGenericRepository<MembershipPermissions> MembershipPermissionsRepository { get; }

        void Save();
        void Dispose();
    }
}
