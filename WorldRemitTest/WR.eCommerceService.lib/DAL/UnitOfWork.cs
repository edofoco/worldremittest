﻿using System;
using WR.eCommerceService.lib.Models;

namespace WR.eCommerceService.lib.DAL
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private StoreContext _context;
        private IGenericRepository<Product> productRepository;
        private IGenericRepository<User> userRepository;
        private IGenericRepository<Membership> membershipRepository;
        private IGenericRepository<Order> orderRepository;
        private IGenericRepository<ProductType> productTypeRepository;
        private IGenericRepository<MembershipPermissions> membershipPermissionsRepository;

        private bool disposed = false;

        public UnitOfWork(StoreContext schoolContext)
        {
            _context = schoolContext;
        }

        public IGenericRepository<Product> ProductRepository
        {
            get
            {
                if (productRepository == null)
                {
                    productRepository = new GenericRepository<Product>(_context);
                }
                return productRepository;
            }
        }

        public IGenericRepository<User> UserRepository
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new GenericRepository<User>(_context);
                }
                return userRepository;
            }
        }
        public IGenericRepository<Membership> MembershipRepository
        {
            get
            {
                if (membershipRepository == null)
                {
                    membershipRepository = new GenericRepository<Membership>(_context);
                }
                return membershipRepository;
            }
        }
        public IGenericRepository<Order> OrderRepository
        {
            get
            {
                if (orderRepository == null)
                {
                    orderRepository = new GenericRepository<Order>(_context);
                }
                return orderRepository;
            }
        }
        public IGenericRepository<ProductType> ProductTypeRepository
        {
            get
            {
                if (productTypeRepository == null)
                {
                    productTypeRepository = new GenericRepository<ProductType>(_context);
                }
                return productTypeRepository;
            }
        }
        public IGenericRepository<MembershipPermissions> MembershipPermissionsRepository
        {
            get
            {
                if (membershipPermissionsRepository == null)
                {
                    membershipPermissionsRepository = new GenericRepository<MembershipPermissions>(_context);
                }
                return membershipPermissionsRepository;
            }
        }


        public void Save()
        {
            _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
