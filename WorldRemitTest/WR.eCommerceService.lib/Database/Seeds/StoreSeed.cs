﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WR.eCommerceService.lib.DAL;
using WR.eCommerceService.lib.Models;

namespace WR.eCommerceService.lib.Database.Seeds
{
    public class StoreSeed: DropCreateDatabaseAlways<StoreContext>
    {
        protected override void Seed(StoreContext context)
        {
            var membershipPermissions = new List<MembershipPermissions>
            {
                new MembershipPermissions{MembershipPermissionId = 1, Permission="Read Books"},
                new MembershipPermissions{MembershipPermissionId = 2, Permission="Watch Videos"},
                new MembershipPermissions{MembershipPermissionId = 3, Permission="All"},
             };

            membershipPermissions.ForEach(p => context.MembershipPermissions.Add(p));
            context.SaveChanges();

            var memberships = new List<Membership>
            {
                new Membership{MembershipId=1, Name="Books Membership", Cost=20.5m, Permissions = new List<MembershipPermissions>()},
                new Membership{MembershipId=2, Name="Videos Membership", Cost=30m, Permissions = new List<MembershipPermissions>()},
                new Membership{MembershipId=3, Name="Premium Membership", Cost=40m, Permissions = new List<MembershipPermissions>()}
            };

            //Add Membership Permissions
            memberships.First(m => m.MembershipId == 1).Permissions.Add(membershipPermissions.First(p => p.MembershipPermissionId == 1));
            memberships.First(m => m.MembershipId == 2).Permissions.Add(membershipPermissions.First(p => p.MembershipPermissionId == 2));
            memberships.First(m => m.MembershipId == 3).Permissions.Add(membershipPermissions.First(p => p.MembershipPermissionId == 3));

            memberships.ForEach(s => context.Memberships.Add(s));
            context.SaveChanges();

            var users = new List<User>
            {
                new User{Name="John",Address_1="Oxford Circus", City="London", Country="UK", Postcode="W1", MembershipId=1},
                new User{Name="Beatrice", Address_1="Shoreditch", City="London", Country="UK", Postcode="E1", MembershipId=2},
                new User{Name="Marco",Address_1="Picadilly Circus", City="London", Country="UK", Postcode="W2", MembershipId=3},
                new User{Name="Smith",Address_1="Picadilly Circus", City="London", Country="UK", Postcode="W2"}

            };

            users.ForEach(s => context.Users.Add(s));
            context.SaveChanges();

            var productTypes = new List<ProductType> {
                new ProductType{ ProductTypeId = 1, Type = "Book", IsPhysical = true},
                new ProductType{ ProductTypeId = 2, Type = "Video", IsPhysical = false}
            };

            productTypes.ForEach(p => context.ProductTypes.Add(p));
            context.SaveChanges();

            var products = new List<Product> {
                new Product{ ProductId = 1, Name = "The Jungle Book", ProductTypeId = 1, Cost = 10.5m},
                new Product{ ProductId = 2, Name = "Iron Man", ProductTypeId = 2, Cost = 9.5m},
                new Product{ ProductId = 3, Name = "Here Gone", ProductTypeId = 1, Cost = 18m},
                new Product{ ProductId = 4, Name = "Harry Potter", ProductTypeId = 2, Cost = 25.6m}
            };

            products.ForEach(p => context.Products.Add(p));
            context.SaveChanges();
        }
    }
}
