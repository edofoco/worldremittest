﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WR.eCommerceService.lib.Models;

namespace WR.eCommerceService.lib
{
    public interface IECommerceService
    {
        List<Product> Products { get; }
        List<Membership> Memberships { get; }
        List<User> Users { get; }
        List<Order> Orders { get; }
        Product GetProduct(int id);
        Membership GetMembership(int id);
        User GetUser(int id);
        void CreateOrder(Order order);
        void UpdateUser(User user);
    }

}
