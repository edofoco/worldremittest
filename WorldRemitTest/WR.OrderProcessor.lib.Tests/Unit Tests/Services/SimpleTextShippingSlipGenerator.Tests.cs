﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WR.eCommerceService.lib.Models;
using WR.OrderProcessor.lib;
using WR.OrderProcessor.lib.Services;

namespace WR.OrderProcessor.lib.Tests.Unit_Tests.Services
{
    [TestClass]
    public class SimpleTextShippingSlipGenerator
    {
        private static Mock<IShippingSlipWriter> _mockShippingSlipWriter;
        private static lib.Services.SimpleTextShippingSlipGenerator SUT;

        private Order CreateDummyOrder() {
            var order = new Order
            {
                User = new User
                {
                    UserId = 1,
                    Name = "John",
                    Address_1 = "Oxford St",
                    Postcode = "W1",
                    City = "City",
                    Country = "UK"
                },
                Products = new List<Product> {
                    new Product { Name = "Book1", Cost = 10.5m, ProductType = new ProductType { Type = "Book", IsPhysical = true} },
                    new Product { Name = "Book2", Cost = 15.5m, ProductType = new ProductType { Type = "Book", IsPhysical = true} }
                },
                OrderId = 1,
                Total = 16,
                UserId = 1
            };

            return order;
        }

        [TestMethod]
        public async Task SimpleTextShippingSlipGenerator_GenerateShippingSlip_VerifyWriteIsCalled()
        {
            _mockShippingSlipWriter = new Mock<IShippingSlipWriter>();
            _mockShippingSlipWriter.Setup(m => m.Write(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.CompletedTask);


            SUT = new lib.Services.SimpleTextShippingSlipGenerator(_mockShippingSlipWriter.Object);

            var order = CreateDummyOrder();
            await SUT.GenerateShippingSlip(order);

            _mockShippingSlipWriter.Verify(m => m.Write(It.IsAny<string>(), It.IsAny<string>()), Times.Once());

        }

        [TestMethod]
        public async Task SimpleTextShippingSlipGenerator_GenerateShippingSlip_VerifyRetryStrategy4Times()
        {
            _mockShippingSlipWriter = new Mock<IShippingSlipWriter>();
            _mockShippingSlipWriter.Setup(m => m.Write(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromException(new Exception("Couldn't write file")));


            SUT = new lib.Services.SimpleTextShippingSlipGenerator(_mockShippingSlipWriter.Object);

            var order = CreateDummyOrder();
            await SUT.GenerateShippingSlip(order);

            _mockShippingSlipWriter.Verify(m => m.Write(It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(4));

        }

    }
    }
