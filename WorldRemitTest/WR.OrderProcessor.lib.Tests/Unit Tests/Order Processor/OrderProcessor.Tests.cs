﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WR.eCommerceService.lib;
using WR.eCommerceService.lib.Models;
using WR.OrderProcessor.lib.Services;

namespace WR.OrderProcessor.lib.Tests.Unit_Tests.Order_Processor
{
    [TestClass]
    public class OrderProcessor
    {
        private static Mock<IECommerceService> _mockECommerceService;
        private static Mock<IShippingSlipGenerator> _mockShippingSlipGenerator;

        private static lib.OrderProcessor SUT;

        private User CreateDummyUser()
        {
            return new User
            {
                UserId = 1,
                Name = "John",
                Address_1 = "Oxford St",
                Postcode = "W1",
                City = "City",
                Country = "UK"
            };
            
        }

        private List<Product> CreateDummyProducts() {
            return new List<Product> {
                    new Product { Name = "Book1", Cost = 10.5m, ProductType = new ProductType { Type = "Book", IsPhysical = true} },
                    new Product { Name = "Book2", Cost = 15.5m, ProductType = new ProductType { Type = "Book", IsPhysical = true} }
                };
        }

        private Membership CreateDummyMembership() {
            return new Membership
            {
                MembershipId = 1,
                Name = "Premium",
                Cost = 30m
            };
        }

        [TestMethod]
        public void OrderProcessor_ProcessOrderWithOnlyProducts_VerifyOrderGeneratedCorrectly()
        {
            _mockECommerceService = new Mock<IECommerceService>();
            _mockShippingSlipGenerator = new Mock<IShippingSlipGenerator>();
            
            SUT = new lib.OrderProcessor(_mockECommerceService.Object, _mockShippingSlipGenerator.Object);

            var user = CreateDummyUser();
            var products = CreateDummyProducts();

            var order = SUT.ProcessOrder(user, products);

            decimal expectedOrderTotal = 0;
            foreach (var product in products) {
                expectedOrderTotal += product.Cost;
            }

            Assert.AreEqual(user.UserId, order.UserId);
            CollectionAssert.AreEqual(products, order.Products.ToList());
            Assert.AreEqual(null, order.MembershipId);
            Assert.AreEqual(expectedOrderTotal, order.Total);

        }

        [TestMethod]
        public void OrderProcessor_ProcessOrderWithOnlyMembership_VerifyOrderGeneratedCorrectly()
        {
            _mockECommerceService = new Mock<IECommerceService>();
            _mockShippingSlipGenerator = new Mock<IShippingSlipGenerator>();

            SUT = new lib.OrderProcessor(_mockECommerceService.Object, _mockShippingSlipGenerator.Object);

            var user = CreateDummyUser();
            var products = new List<Product>();
            var membership = CreateDummyMembership();

            var order = SUT.ProcessOrder(user, products, membership);

            decimal expectedOrderTotal = membership.Cost;
            

            Assert.AreEqual(user.UserId, order.UserId);
            Assert.AreEqual(membership.MembershipId, order.MembershipId);
            CollectionAssert.AreEqual(products, order.Products.ToList());
            Assert.AreEqual(expectedOrderTotal, order.Total);
        }

        [TestMethod]
        public void OrderProcessor_ProcessOrderWithProductsAndMembership_VerifyOrderGeneratedCorrectly()
        {
            _mockECommerceService = new Mock<IECommerceService>();
            _mockShippingSlipGenerator = new Mock<IShippingSlipGenerator>();

            SUT = new lib.OrderProcessor(_mockECommerceService.Object, _mockShippingSlipGenerator.Object);

            var user = CreateDummyUser();
            var products = CreateDummyProducts();
            var membership = CreateDummyMembership();

            var order = SUT.ProcessOrder(user, products, membership);

            decimal expectedOrderTotal = membership.Cost;
            foreach (var product in products)
            {
                expectedOrderTotal += product.Cost;
            }

            Assert.AreEqual(user.UserId, order.UserId);
            Assert.AreEqual(membership.MembershipId, order.MembershipId);
            CollectionAssert.AreEqual(products, order.Products.ToList());
            Assert.AreEqual(expectedOrderTotal, order.Total);

        }

    }
}
