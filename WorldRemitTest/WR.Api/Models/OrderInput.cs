﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WR.Api.Models
{
    public class OrderInput
    {
        [Required]
        public List<int> ProductsIds { get; set; }
        public int? MembershipId { get; set; }
    }
}