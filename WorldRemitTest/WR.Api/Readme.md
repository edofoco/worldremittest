# World Remit Test
This is a .Net backend API and Librarires for the FunBooksAndVideos e-commerce shop.

## System Requirements
- .Net 4.6

- LocalDB (https://docs.microsoft.com/en-us/sql/database-engine/configure-windows/sql-server-2016-express-localdb)

## Used Libraries
- Structuremap.WebApi2 (IoC)
- EntityFramework 6
- Moq

## Configuration (Web.Config)
The only AppSetting that needs modification prior to running the solution is:

`<add key="ShippingSlipWriter.BasePath" value="C:\Users\Edoardo Foco\Desktop\Orders" />`

Which contains the base path for writing the ShippingSlip.


## Project Structure
To ensure maximum reusability of the components the solution has been structured in 3 different projects.
- WR.Api - is responsible for providing access to the system through an API layer:
    - Handling input validation
    - Handling exceptions
    - Does not include Authentication/Authorzation systems as these have been scoped out for this project
- WR.eCommerceService.lib - is responsible for providing a CRUD interface to the data model.
- WR.OrderProcessor.lib - is responsbile for the order processing logic

## Database Seeding
When running the solution in Visual Studio the application will seed the database to provide an initial Data Set. It will create:

- 3 Memberships
    - Book Membership
    - Video Membership
    - Premium Membership
- 4 Users
    - John - Linked to a Book membership
    - Beatrice - Linked to a Video membership
    - Marco - Linked to a Premium membership
    - Smith - Linked to no membership
- 4 Products
    - Jungle Book [Book]
    - Iron Man [Video]
    - Here Gone [Book]
    - Harry Potter [Video]

..Other tables are create as well and haven't been included int his description as they simply create context for the main tables above.

## CORS
This API supports CORS.

## API Endpoints
Note - For the scope of this project the only endpoints that were generated are the ones to read the state of the system and to create an order on behalf of a user.

### Users
- GET /api/users
- POST /api/users/{id}/orders
    Parameters:
    - ProductsIds - Array(int), Required
    - MembershipId - int
    
    Sample Request
    `{ "productsIds": [1,2], "membership": 1 }`

### Products
- GET /api/products

### Memberships
- GET /api/memberships

### Orders
- GET /api/orders


