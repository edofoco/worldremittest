﻿using System;

namespace WR.Api.Exceptions
{ 
    public class BadRequestException : Exception {

        public BadRequestException(string message):base(message)
        {

        }
    }
}