﻿using System;
using System.Collections.Generic;
using WR.Api.Exceptions;
using WR.eCommerceService.lib;
using WR.eCommerceService.lib.Models;
using WR.OrderProcessor.lib;

namespace WR.Api.Services
{
    public class OrderService: IOrderService
    {
        private readonly IECommerceService _eCommerceService;
        private readonly IOrderProcessor _orderProcessor;

        public OrderService(IECommerceService eCommerceService, IOrderProcessor orderProcessor)
        {
            _eCommerceService = eCommerceService;
            _orderProcessor = orderProcessor;
        }

        public Order CreateOrder(int userId, List<int> productsIds, int? membershipId)
        {
            var user = _eCommerceService.GetUser(userId);
            if (user == null)
            {
                throw new BadRequestException("User not found");
            }

            var products = new List<Product>();
            foreach (var productId in productsIds)
            {
                var product = _eCommerceService.GetProduct(productId);
                if (product == null)
                {
                    throw new BadRequestException($"Unable to find product with Id: {productId}");
                }

                products.Add(product);
            }

            Membership membership = null;

            if (membershipId != null)
            {
                membership = _eCommerceService.GetMembership((int) membershipId);
            }

            return _orderProcessor.ProcessOrder(user, products, membership);
        }

        public List<Order> GetOrders()
        {
            throw new NotImplementedException();
        }
    }
}