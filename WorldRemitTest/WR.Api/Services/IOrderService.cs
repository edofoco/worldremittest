﻿using System.Collections.Generic;
using WR.eCommerceService.lib.Models;

namespace WR.Api.Services
{
    public interface IOrderService
    {
        List<Order> GetOrders();
        Order CreateOrder(int userId, List<int> products, int? membershipId);
    }
}