// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultRegistry.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WR.Api.DependencyResolution {
    using StructureMap.Configuration.DSL;
    using StructureMap.Graph;
    using System.Configuration;
    using WR.Api.Services;
    using WR.eCommerceService.lib;
    using WR.eCommerceService.lib.DAL;
    using WR.OrderProcessor.lib;
    using WR.OrderProcessor.lib.Services;

    public class DefaultRegistry : Registry {
        #region Constructors and Destructors

        public DefaultRegistry() {
            Scan(
                scan => {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });
            For<IUnitOfWork>().Use<UnitOfWork>();
            For<IECommerceService>().Use<ECommerceService>();
            For<IOrderProcessor>().Use<OrderProcessor>();
            For<IOrderService>().Use<OrderService>();
            For<IShippingSlipGenerator>().Use<SimpleTextShippingSlipGenerator>();
            For<IShippingSlipWriter>().Use<FileWriter>()
                .Ctor<string>("basePath").Is(ConfigurationManager.AppSettings["ShippingSlipWriter.BasePath"]);

        }

        #endregion
    }
}