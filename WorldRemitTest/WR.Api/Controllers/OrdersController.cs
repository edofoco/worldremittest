﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WR.eCommerceService.lib;
using WR.eCommerceService.lib.Models;
using WR.OrderProcessor.lib;

namespace WR.Api.Controllers
{
    public class OrdersController : ApiController
    {
        private readonly IECommerceService _eCommerceService;
        private readonly IOrderProcessor _orderProcessor;

        public OrdersController(IECommerceService eCommerceService, IOrderProcessor orderProcessor)
        {
            _eCommerceService = eCommerceService;
            _orderProcessor = orderProcessor;
        }

        // GET api/orders
        public IEnumerable<Order> Get()
        {
            return _eCommerceService.Orders;
        }

        // POST api/orders
        public void Post([FromBody]string value)
        {
            
        }
    }
}
