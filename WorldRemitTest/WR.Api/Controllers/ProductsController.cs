﻿using System.Collections.Generic;
using System.Web.Http;
using WR.eCommerceService.lib;
using WR.eCommerceService.lib.Models;

namespace WR.Api.Controllers
{
    public class ProductsController : ApiController
    {
        IECommerceService _eCommerceService;

        public ProductsController(IECommerceService eCommerceService) {
            _eCommerceService = eCommerceService;
        }

        // GET api/products
        public IEnumerable<Product> Get()
        {
            return _eCommerceService.Products;
        }
    }
}
