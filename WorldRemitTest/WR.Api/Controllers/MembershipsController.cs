﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WR.eCommerceService.lib;
using WR.eCommerceService.lib.Models;

namespace WR.Api.Controllers
{
    public class MembershipsController : ApiController
    {
        IECommerceService _eCommerceService;

        public MembershipsController(IECommerceService eCommerceService)
        {
            _eCommerceService = eCommerceService;
        }

        // GET api/memberships
        public IEnumerable<Membership> Get()
        {
            return _eCommerceService.Memberships;
        }
    }
}
