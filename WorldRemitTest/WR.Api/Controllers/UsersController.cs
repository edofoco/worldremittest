﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WR.Api.Exceptions;
using WR.Api.Models;
using WR.Api.Services;
using WR.eCommerceService.lib;
using WR.eCommerceService.lib.Models;

namespace WR.Api.Controllers
{
    public class UsersController : ApiController
    {
        private readonly IECommerceService _eCommerceService;
        private readonly IOrderService _orderService;

        public UsersController(IECommerceService eCommerceService, IOrderService orderService)
        {
            _eCommerceService = eCommerceService;
            _orderService = orderService;
        }

        // GET api/users
        public IEnumerable<User> Get()
        {
            return _eCommerceService.Users;
        }

        // POST api/users/1/orders
        [Route("api/users/{id}/orders")]
        [HttpPost]
        public IHttpActionResult Orders(int id, [FromBody]OrderInput value)
        {
            try {
                //Verify user is authorized - Authorization system is not implemented yet
                if (ModelState.IsValid)
                {
                    if (value.MembershipId == null && value.ProductsIds.Count == 0) {
                        throw new BadRequestException("At least one product or membership must be specified.");
                    }
                    
                    return Ok(_orderService.CreateOrder(id, value.ProductsIds, value.MembershipId));
                }
                else
                {
                    return BadRequest(ModelState);
                }
            } catch (BadRequestException e) {
                return BadRequest(e.Message);
            }

        }


    }
}
